﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using QuotesAPI.BAL.DTO;
using QuotesAPI.BAL.Services.Interfaces;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuotesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorsController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        // GET: api/<AuthorsController>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_authorService.GetAll());
            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }

        // GET api/<AuthorsController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                AuthorDTO author = _authorService.Get(id);

                if (author == null)
                {
                    return NotFound();
                }
                return Ok(author);

            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }

        // POST api/<AuthorsController>
        [HttpPost]
        public IActionResult Post([FromBody][Bind("Name")] AuthorDTO author)
        {
            try
            {
                if (author == null)
                    return BadRequest();

                // Creates an author and retrieves it's Id
                int id = _authorService.Create(author);

                return Ok(id);
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(500);
            }
        }

        // PUT api/<AuthorsController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] AuthorDTO author)
        {
            try
            {
                if (author == null || id != author.Id)
                    return BadRequest();

                //Attempts to update author
                int resId = _authorService.Update(author);

                //Author not found in database
                if (resId == -1)
                    return NotFound();

                return NoContent();
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(500);
            }
        }

        // DELETE api/<AuthorsController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                //Attempts a delete on an author id
                bool success = _authorService.Delete(id);

                if (!success)
                    return NotFound();

                return NoContent();
            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
