﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using QuotesAPI.BAL.DTO;
using QuotesAPI.BAL.Services.Interfaces;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuotesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {
        private readonly IQuoteService _quoteService;

        public QuotesController(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        // GET: api/<QuotesController>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_quoteService.GetAll());
            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }

        // GET: api/<QuotesController>
        [HttpGet("getrandom")]
        public IActionResult GetRandom()
        {
            try
            {
                return Ok(_quoteService.GetRandom());
            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }

        // GET api/<QuotesController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                QuoteDTO quote = _quoteService.Get(id);

                if (quote == null)
                {
                    return NotFound();
                }
                return Ok(quote);

            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }

        // POST api/<QuotesController>
        [HttpPost]
        public IActionResult Post([FromBody][Bind("AuthorId,Content")] QuoteDTO quote)
        {
            try
            {
                if (quote == null)
                    return BadRequest();

                // Creates an quote and retrieves it's Id
                int id = _quoteService.Create(quote);

                return Ok(id);
            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }

        // PUT api/<QuotesController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] QuoteDTO quote)
        {
            try
            {
                if (quote == null || id != quote.Id)
                    return BadRequest();

                //Attempts to update quote
                int resId = _quoteService.Update(quote);

                //Quote not found in database
                if (resId == -1)
                    return NotFound();

                return NoContent();
            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }

        // DELETE api/<QuotesController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                //Attempts a delete on an quote id
                bool success = _quoteService.Delete(id);

                if (!success)
                    return NotFound();

                return NoContent();
            }
            catch (Exception)
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
