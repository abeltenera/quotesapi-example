﻿using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuotesAPI.BAL.DTO
{
    public class QuoteDTO
    {
        public QuoteDTO() { }
        public QuoteDTO(Quote quote)
        {
            this.Id = quote.Id;
            this.AuthorId = quote.AuthorId;
            this.Content = quote.Content;
            if (quote.Author != null)
                this.Author = new AuthorDTO(quote.Author);
        }

        public int Id { get; set; }
        public int AuthorId { get; set; }
        public string Content { get; set; }

        public AuthorDTO Author { get; set; }

        public Quote ToEntity()
        {
            return new Quote()
            {
                AuthorId = AuthorId,
                Content = Content,
                Id = Id
            };
        }
    }
}
