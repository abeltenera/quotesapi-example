﻿using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuotesAPI.BAL.DTO
{
    public class AuthorDTO
    {
        public AuthorDTO()
        {
        }

        public AuthorDTO(Author author)
        {
            this.Id = author.Id;
            this.Name = author.Name;           
        }

        public int Id { get; set; }
        public string Name { get; set; } 


        public Author ToEntity()
        {
            return new Author()
            {
                Id = Id,
                Name = Name
            };
        }
    }
}
