﻿using QuotesAPI.BAL.Services.Interfaces;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using QuotesAPI.BAL.DTO;

namespace QuotesAPI.BAL.Services
{
    public class QuoteService : IQuoteService
    {
        static Random rnd = new Random();

        private readonly quotesdbContext _context;
        public QuoteService(quotesdbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates an quote
        /// </summary>
        /// <param name="quote"></param>
        /// <returns>Id of the quote created</returns>
        public int Create(QuoteDTO quote)
        {
            Quote entity = quote.ToEntity();
            entity.CreationTs = DateTime.Now;
            _context.Add(entity);
            _context.SaveChanges();

            return entity.Id;
        }


        /// <summary>
        /// Attempts a delete on an given quote id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if quote exists, false if not</returns>
        public bool Delete(int id)
        {
            Quote quote = _context.Quote.AsNoTracking().FirstOrDefault(a => a.Id == id);
            if (quote != null)
            {
                _context.Remove(quote);
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Gets a specific quote
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Quote if exists. Null if does not exist</returns>
        public QuoteDTO Get(int id)
        {
            QuoteDTO quote = new QuoteDTO(_context.Quote.Include(q => q.Author).FirstOrDefault(a => a.Id == id));
            return quote;
        }

        /// <summary>
        /// Gets all quotes
        /// </summary>
        /// <returns>List of existing quotes</returns>
        public IEnumerable<QuoteDTO> GetAll()
        {
            return _context.Quote.AsNoTracking().Include(q=>q.Author).Select(q=>new QuoteDTO(q)).ToList();
        }

        public QuoteDTO GetRandom()
        {
            IEnumerable<QuoteDTO> quotes = GetAll();
            int numQuotes = quotes.Count();
            int next = rnd.Next(0, numQuotes);
            return quotes.ElementAt(next);
        }

        /// <summary>
        /// Attempts an update on an quote. 
        /// </summary>
        /// <param name="quote"></param>
        /// <returns>Quote id, or -1 if quote does not exist</returns>
        public int Update(QuoteDTO quote)
        {
            Quote existingQuote = _context.Quote.AsNoTracking().FirstOrDefault(a => a.Id == quote.Id);
            if (existingQuote != null)
            {
                Quote entity = quote.ToEntity();
                entity.CreationTs = existingQuote.CreationTs;
                _context.Update(quote.ToEntity());
                _context.SaveChanges();
                return quote.Id;
            }
            else
                return -1;
        }
    }
}
