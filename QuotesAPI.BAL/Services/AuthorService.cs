﻿using QuotesAPI.BAL.Services.Interfaces;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using QuotesAPI.BAL.DTO;
using Microsoft.EntityFrameworkCore;

namespace QuotesAPI.BAL.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly quotesdbContext _context;
        public AuthorService(quotesdbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates an author
        /// </summary>
        /// <param name="author"></param>
        /// <returns>Id of the author created</returns>
        public int Create(AuthorDTO author)
        {
            Author entity = author.ToEntity();
            entity.CreationTs = DateTime.Now;
            _context.Add(entity);
            _context.SaveChanges();

            return entity.Id;
        }


        /// <summary>
        /// Attempts a delete on an given author id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if author exists, false if not</returns>
        public bool Delete(int id)
        {
            Author author = _context.Author.AsNoTracking().FirstOrDefault(a => a.Id == id);
            if (author != null)
            {
                _context.Remove(author);
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Gets a specific author
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Author if exists. Null if does not exist</returns>
        public AuthorDTO Get(int id)
        {
            AuthorDTO author = new AuthorDTO(_context.Author.FirstOrDefault(a => a.Id == id));
            return author;
        }

        /// <summary>
        /// Gets all authors
        /// </summary>
        /// <returns>List of existing authors</returns>
        public IEnumerable<AuthorDTO> GetAll()
        {
            return _context.Author.Select(a=>new AuthorDTO(a)).ToList();
        }

        /// <summary>
        /// Attempts an update on an author. 
        /// </summary>
        /// <param name="author"></param>
        /// <returns>Author id, or -1 if author does not exist</returns>
        public int Update(AuthorDTO author)
        {
            Author existing = _context.Author.AsNoTracking().FirstOrDefault(a => a.Id == author.Id);
            if ( existing != null)
            {
                Author entity = author.ToEntity();
                entity.CreationTs = existing.CreationTs;
                _context.Update(entity);
                _context.SaveChanges();
                return entity.Id;
            }
            else
                return -1;
        }

    }
}
