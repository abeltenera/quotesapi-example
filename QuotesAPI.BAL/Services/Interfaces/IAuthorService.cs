﻿using QuotesAPI.BAL.DTO;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuotesAPI.BAL.Services.Interfaces
{
    public interface IAuthorService
    {
        IEnumerable<AuthorDTO> GetAll();
        AuthorDTO Get(int id);

        int Create(AuthorDTO author);
        int Update(AuthorDTO author);
        bool Delete(int id);
    }
}
