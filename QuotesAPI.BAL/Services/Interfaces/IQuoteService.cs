﻿using QuotesAPI.BAL.DTO;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuotesAPI.BAL.Services.Interfaces
{
    public interface IQuoteService
    {
        IEnumerable<QuoteDTO> GetAll();
        QuoteDTO Get(int id);
        QuoteDTO GetRandom();

        int Create(QuoteDTO author);
        int Update(QuoteDTO author);
        bool Delete(int id);
    }
}
