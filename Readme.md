# Quotes API

## Overview
The whole solution consists on four projects
- QuotesAPI: contains the publicly available API to manage Quote and Author data
- QuotesAPI.BAL: Business Access Layer, that contains DTOs for the database entities, business logic and service interfaces
- QuotesAPI.DAL: Data Access Layer, that contains the database entities and the logic for accessing the database
- QuotesAPI.Tests: Contains the mocks for the quotes and author services, and tests for the API controllers

## Requirements
This project is made under .NetCore 3.1, using MySQL Server 5.6. 

# Other considerations
For future improvements add logging system. 
