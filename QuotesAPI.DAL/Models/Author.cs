﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace QuotesAPI.DAL.Models
{
    public partial class Author
    {
        public Author()
        {
            Quote = new HashSet<Quote>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationTs { get; set; }

        public virtual ICollection<Quote> Quote { get; set; }
    }
}
