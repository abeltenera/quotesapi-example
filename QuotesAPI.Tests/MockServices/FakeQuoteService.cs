﻿using QuotesAPI.BAL.DTO;
using QuotesAPI.BAL.Services.Interfaces;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuotesAPI.Tests.MockServices
{
    public class FakeQuoteService : IQuoteService
    {
        readonly QuoteDTO _quote;

        readonly List<QuoteDTO> _quotes;


        public enum Mode
        {
            SUCCESS,
            FAIL,
            EXCEPTION
        }

        public Mode TestMode { get; set; }
        private readonly int _defaultId;

        public FakeQuoteService( int defaultId = 1)
        { 
            _defaultId = defaultId;

            _quote = new QuoteDTO(){Id = defaultId,AuthorId = 1,Content = "Lorem Ipsum"};

            _quotes = new List<QuoteDTO>() {
                new QuoteDTO(){Id = defaultId,AuthorId = 1,Content = "Lorem Ipsum"},
                new QuoteDTO(){Id = defaultId,AuthorId = 2,Content = "Lorem Ipsum1"},
                new QuoteDTO(){Id = defaultId,AuthorId = 3,Content = "Lorem Ipsum2"},
                new QuoteDTO(){Id = defaultId,AuthorId = 4,Content = "Lorem Ipsum3"},
            };
        }

        public int Create(QuoteDTO quote)
        {
            return TestMode switch
            {
                Mode.SUCCESS => _defaultId,
                _ => throw new Exception(),
            };
        }

        public bool Delete(int id)
        {
            return TestMode switch
            {
                Mode.SUCCESS => true,
                Mode.FAIL => false,
                _ => throw new Exception(),
            };
        }

        public QuoteDTO Get(int id)
        {
            return TestMode switch
            {
                Mode.SUCCESS => _quote,
                Mode.FAIL => null,
                _ => throw new Exception(),
            };
        }

        public QuoteDTO GetRandom()
        {
            return TestMode switch
            {
                Mode.SUCCESS => _quote,
                _ => throw new Exception(),
            };
        }


        public IEnumerable<QuoteDTO> GetAll()
        {
            return TestMode switch
            {
                Mode.SUCCESS => _quotes,
                _ => throw new Exception(),
            };
        }

        public int Update(QuoteDTO quote)
        {
            return TestMode switch
            {
                Mode.SUCCESS => _defaultId,
                Mode.FAIL => -1,
                _ => throw new Exception(),
            };
        }


        public QuoteDTO TestQuote()
        {
            return _quote;
        }


    }
}
