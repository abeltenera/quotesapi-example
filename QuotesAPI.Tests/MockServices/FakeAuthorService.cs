﻿using QuotesAPI.BAL.DTO;
using QuotesAPI.BAL.Services.Interfaces;
using QuotesAPI.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuotesAPI.Tests.MockServices
{
    public class FakeAuthorService : IAuthorService
    {
        readonly AuthorDTO _author;

        readonly List<AuthorDTO> _authors;


        public enum Mode
        {
            SUCCESS,
            FAIL,
            EXCEPTION
        }

        public Mode TestMode { get; set; }
        private readonly int _defaultId;

        public FakeAuthorService( int defaultId = 1)
        { 
            _defaultId = defaultId;

            _author = new AuthorDTO()
            {
                Id = defaultId,
                Name="John Doe"
            };

            _authors = new List<AuthorDTO>() {
                new AuthorDTO() {Id=1, Name = "John Doe" },
                new AuthorDTO() {Id=2, Name = "John Doe2" },
                new AuthorDTO() {Id=3, Name = "John Doe3" },
                new AuthorDTO() {Id=4, Name = "John Doe4" },
            };
        }

        public int Create(AuthorDTO author)
        {
            return TestMode switch
            {
                Mode.SUCCESS => _defaultId,
                _ => throw new Exception(),
            };
        }

        public bool Delete(int id)
        {
            return TestMode switch
            {
                Mode.SUCCESS => true,
                Mode.FAIL => false,
                _ => throw new Exception(),
            };
        }

        public AuthorDTO Get(int id)
        {
            return TestMode switch
            {
                Mode.SUCCESS => _author,
                Mode.FAIL => null,
                _ => throw new Exception(),
            };
        }

        public IEnumerable<AuthorDTO> GetAll()
        {
            return TestMode switch
            {
                Mode.SUCCESS => _authors,
                _ => throw new Exception(),
            };
        }

        public int Update(AuthorDTO author)
        {
            return TestMode switch
            {
                Mode.SUCCESS => _defaultId,
                Mode.FAIL => -1,
                _ => throw new Exception(),
            };
        }


        public AuthorDTO TestAuthor()
        {
            return _author;
        }
    }
}
