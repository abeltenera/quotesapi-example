using Microsoft.AspNetCore.Mvc;
using QuotesAPI.BAL.DTO;
using QuotesAPI.Controllers;
using QuotesAPI.DAL.Models;
using QuotesAPI.Tests.MockServices;
using System;
using System.Collections.Generic;
using Xunit;

namespace QuotesAPI.Tests
{
    public class AuthorControllerTests
    {
        private readonly int _defaultId = 1;
        private readonly FakeAuthorService _fakeAuthorService;
        private readonly AuthorsController _authorsController;

        public AuthorControllerTests()
        {
            _fakeAuthorService = new FakeAuthorService(_defaultId);
            _authorsController = new AuthorsController(_fakeAuthorService);
        }
        #region Get method tests
        /// <summary>
        /// Checks if, in the case of returning data, an Ok result occurs
        /// </summary>
        [Fact]
        public void Get_All_Items_Returns_OkResult_WithContent_Success()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.SUCCESS;


            var result = _authorsController.Get();
            OkObjectResult okRes = result as OkObjectResult;
            //Asserts if returns ok result
            Assert.IsType<OkObjectResult>(okRes);

            //Asserts if returns content of type List<Authors>
            Assert.IsType<List<AuthorDTO>>(okRes.Value);
        }

        /// <summary>
        /// Checks if, in the case of exception in service, an error 500 result occurs
        /// </summary>
        [Fact]
        public void Get_All_Items_Returns_InternalServerError_Exception()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.EXCEPTION;


            StatusCodeResult result = (StatusCodeResult)_authorsController.Get();
            Assert.Equal(500, result.StatusCode);
        }

        /// <summary>
        /// Checks if, in the case of returning data, an Ok result occurs
        /// </summary>
        [Fact]
        public void Get_Item_By_Id_Returns_OkResult_WithContent_Success()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.SUCCESS;


            var result = _authorsController.Get(_defaultId);

            OkObjectResult okRes = result as OkObjectResult;
            //Asserts if returns ok result
            Assert.IsType<OkObjectResult>(okRes);

            //Asserts if returns content of type List<Authors>
            Assert.IsType<AuthorDTO>(okRes.Value);
        }

        /// <summary>
        /// Checks if, in the case of exception in service, an error 500 result occurs
        /// </summary>
        [Fact]
        public void Get_Item_By_Id_Returns_InternalServerError_Exception()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.EXCEPTION;


            StatusCodeResult result = (StatusCodeResult)_authorsController.Get(_defaultId);
            Assert.Equal(500, result.StatusCode);
        }

        #endregion

        #region Post method tests

        /// <summary>
        /// Checks if, the author is null, post method returns bad request
        /// </summary>
        [Fact]
        public void Post_NullAuthor_BadRequest()
        {


            var result = _authorsController.Post(null);
            Assert.IsType<BadRequestResult>(result as BadRequestResult);
        }

        /// <summary>
        /// Checks if, the author exists, post method returns ok result with id
        /// </summary>
        [Fact]
        public void Post_AuthorCreated_OkResult_With_Id()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.SUCCESS;

            var result = _authorsController.Post(_fakeAuthorService.TestAuthor()) as OkObjectResult;
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<int>(result.Value);
            Assert.Equal(_defaultId, result.Value);
        }

        /// <summary>
        /// Checks if, in case of exception, post method returns error 500
        /// </summary>
        [Fact]
        public void Post_Exception_InternalServerError()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.EXCEPTION;

            var result = _authorsController.Post(_fakeAuthorService.TestAuthor()) as StatusCodeResult;
            Assert.Equal(500, result.StatusCode);
        }

        #endregion

        #region Put method tests
        /// <summary>
        /// Checks if, the author exists and input Id equals author Id, put method returns no content
        /// </summary>
        [Fact]
        public void Put_Author_NotNull_Id_Match_NoContent()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.SUCCESS;

            var result = _authorsController.Put(_defaultId, _fakeAuthorService.TestAuthor()) as NoContentResult;
            Assert.IsType<NoContentResult>(result);
        }

        /// <summary>
        /// Checks if, the ids don't match or author is null, returns bad request
        /// </summary>
        [Fact]
        public void Put_Author_AuthorNull_Or_Id_Not_Match_BadRequest()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.SUCCESS;
            // Non matching ids test
            var result = _authorsController.Put(_defaultId + 1, _fakeAuthorService.TestAuthor()) as BadRequestResult;
            Assert.IsType<BadRequestResult>(result);

            // Null test
            var result2 = _authorsController.Put(_defaultId, null) as BadRequestResult;
            Assert.IsType<BadRequestResult>(result);
        }


        /// <summary>
        /// Checks if, the author does not exists, returns Not found
        /// </summary>
        [Fact]
        public void Put_Author_Not_Exists_NotFound()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.FAIL;

            var result = _authorsController.Put(_defaultId, _fakeAuthorService.TestAuthor()) as NotFoundResult;
            Assert.IsType<NotFoundResult>(result);
        }

        /// <summary>
        /// Checks if, in case of exception, returns error 500
        /// </summary>
        [Fact]
        public void Put_Exception_InternalServerError()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.EXCEPTION;

            var result = _authorsController.Put(_defaultId, _fakeAuthorService.TestAuthor()) as StatusCodeResult;
            Assert.Equal(500, result.StatusCode);
        }


        #endregion

        #region Delete method tests

        /// <summary>
        /// Checks if, the author exists, delete method returns no content
        /// </summary>
        [Fact]
        public void Delete_Id_Exists_NoContent()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.SUCCESS;

            var result = _authorsController.Delete(_defaultId ) as NoContentResult;
            Assert.IsType<NoContentResult>(result);
        }

        /// <summary>
        /// Checks if, the author not exists, delete method returns not found
        /// </summary>
        [Fact]
        public void Delete_Id_Not_Exists_NotFound()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.FAIL;

            var result = _authorsController.Delete(_defaultId) as NotFoundResult;
            Assert.IsType<NotFoundResult>(result);
        }

        /// <summary>
        /// Checks if, in case of exception, delete method returns error 500
        /// </summary>
        [Fact]
        public void Delete_Exception_InternalServerError()
        {
            _fakeAuthorService.TestMode = FakeAuthorService.Mode.EXCEPTION;

            var result = _authorsController.Delete(_defaultId) as StatusCodeResult;
            Assert.Equal(500,result.StatusCode);
        }
        #endregion
    }
}
