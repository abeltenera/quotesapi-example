using Microsoft.AspNetCore.Mvc;
using QuotesAPI.BAL.DTO;
using QuotesAPI.Controllers;
using QuotesAPI.DAL.Models;
using QuotesAPI.Tests.MockServices;
using System;
using System.Collections.Generic;
using Xunit;

namespace QuotesAPI.Tests
{
    public class QuoteControllerTests
    {
        private readonly int _defaultId = 1;
        private readonly FakeQuoteService _fakeQuoteService;
        private readonly QuotesController _quotesController;

        public QuoteControllerTests()
        {
            _fakeQuoteService = new FakeQuoteService(_defaultId);
            _quotesController = new QuotesController(_fakeQuoteService);
        }
        #region Get method tests
        /// <summary>
        /// Checks if, in the case of returning data, an Ok result occurs
        /// </summary>
        [Fact]
        public void Get_All_Items_Returns_OkResult_WithContent_Success()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.SUCCESS;


            var result = _quotesController.Get();
            OkObjectResult okRes = result as OkObjectResult;
            //Asserts if returns ok result
            Assert.IsType<OkObjectResult>(okRes);

            //Asserts if returns content of type List<Quotes>
            Assert.IsType<List<QuoteDTO>>(okRes.Value);
        }

        /// <summary>
        /// Checks if, in the case of exception in service, an error 500 result occurs
        /// </summary>
        [Fact]
        public void Get_All_Items_Returns_InternalServerError_Exception()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.EXCEPTION;


            StatusCodeResult result = (StatusCodeResult)_quotesController.Get();
            Assert.Equal(500, result.StatusCode);
        }

        /// <summary>
        /// Checks if, in the case of returning data, an Ok result occurs
        /// </summary>
        [Fact]
        public void Get_Item_By_Id_Returns_OkResult_WithContent_Success()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.SUCCESS;


            var result = _quotesController.Get(_defaultId);

            OkObjectResult okRes = result as OkObjectResult;
            //Asserts if returns ok result
            Assert.IsType<OkObjectResult>(okRes);

            //Asserts if returns content of type List<Quotes>
            Assert.IsType<QuoteDTO>(okRes.Value);
        }

        /// <summary>
        /// Checks if, in the case of exception in service, an error 500 result occurs
        /// </summary>
        [Fact]
        public void Get_Item_By_Id_Returns_InternalServerError_Exception()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.EXCEPTION;


            StatusCodeResult result = (StatusCodeResult)_quotesController.Get(_defaultId);
            Assert.Equal(500, result.StatusCode);
        }

        #endregion

        #region Get Random tests

        /// <summary>
        /// Checks if, in the case of returning data, an Ok result occurs
        /// </summary>
        [Fact]
        public void GetRandom_Returns_OkResult_WithContent_Success()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.SUCCESS;


            var result = _quotesController.GetRandom();

            OkObjectResult okRes = result as OkObjectResult;
            //Asserts if returns ok result
            Assert.IsType<OkObjectResult>(okRes);

            //Asserts if returns content of type List<Quotes>
            Assert.IsType<QuoteDTO>(okRes.Value);
        }

        /// <summary>
        /// Checks if, in the case of exception in service, an error 500 result occurs
        /// </summary>
        [Fact]
        public void GetRandom_Returns_InternalServerError_Exception()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.EXCEPTION;


            StatusCodeResult result = (StatusCodeResult)_quotesController.GetRandom();
            Assert.Equal(500, result.StatusCode);
        }

        #endregion

        #region Post method tests

        /// <summary>
        /// Checks if, the quote is null, post method returns bad request
        /// </summary>
        [Fact]
        public void Post_NullQuote_BadRequest()
        {


            var result = _quotesController.Post(null);
            Assert.IsType<BadRequestResult>(result as BadRequestResult);
        }

        /// <summary>
        /// Checks if, the quote exists, post method returns ok result with id
        /// </summary>
        [Fact]
        public void Post_QuoteCreated_OkResult_With_Id()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.SUCCESS;

            var result = _quotesController.Post(_fakeQuoteService.TestQuote()) as OkObjectResult;
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<int>(result.Value);
            Assert.Equal(_defaultId, result.Value);
        }

        /// <summary>
        /// Checks if, in case of exception, post method returns error 500
        /// </summary>
        [Fact]
        public void Post_Exception_InternalServerError()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.EXCEPTION;

            var result = _quotesController.Post(_fakeQuoteService.TestQuote()) as StatusCodeResult;
            Assert.Equal(500, result.StatusCode);
        }

        #endregion

        #region Put method tests
        /// <summary>
        /// Checks if, the quote exists and input Id equals quote Id, put method returns no content
        /// </summary>
        [Fact]
        public void Put_Quote_NotNull_Id_Match_NoContent()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.SUCCESS;

            var result = _quotesController.Put(_defaultId, _fakeQuoteService.TestQuote()) as NoContentResult;
            Assert.IsType<NoContentResult>(result);
        }

        /// <summary>
        /// Checks if, the ids don't match or quote is null, returns bad request
        /// </summary>
        [Fact]
        public void Put_Quote_QuoteNull_Or_Id_Not_Match_BadRequest()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.SUCCESS;
            // Non matching ids test
            var result = _quotesController.Put(_defaultId + 1, _fakeQuoteService.TestQuote()) as BadRequestResult;
            Assert.IsType<BadRequestResult>(result);

            // Null test
            var result2 = _quotesController.Put(_defaultId, null) as BadRequestResult;
            Assert.IsType<BadRequestResult>(result);
        }


        /// <summary>
        /// Checks if, the quote does not exists, returns Not found
        /// </summary>
        [Fact]
        public void Put_Quote_Not_Exists_NotFound()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.FAIL;

            var result = _quotesController.Put(_defaultId, _fakeQuoteService.TestQuote()) as NotFoundResult;
            Assert.IsType<NotFoundResult>(result);
        }

        /// <summary>
        /// Checks if, in case of exception, returns error 500
        /// </summary>
        [Fact]
        public void Put_Exception_InternalServerError()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.EXCEPTION;

            var result = _quotesController.Put(_defaultId, _fakeQuoteService.TestQuote()) as StatusCodeResult;
            Assert.Equal(500, result.StatusCode);
        }


        #endregion

        #region Delete method tests

        /// <summary>
        /// Checks if, the quote exists, delete method returns no content
        /// </summary>
        [Fact]
        public void Delete_Id_Exists_NoContent()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.SUCCESS;

            var result = _quotesController.Delete(_defaultId ) as NoContentResult;
            Assert.IsType<NoContentResult>(result);
        }

        /// <summary>
        /// Checks if, the quote not exists, delete method returns not found
        /// </summary>
        [Fact]
        public void Delete_Id_Not_Exists_NotFound()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.FAIL;

            var result = _quotesController.Delete(_defaultId) as NotFoundResult;
            Assert.IsType<NotFoundResult>(result);
        }

        /// <summary>
        /// Checks if, in case of exception, delete method returns error 500
        /// </summary>
        [Fact]
        public void Delete_Exception_InternalServerError()
        {
            _fakeQuoteService.TestMode = FakeQuoteService.Mode.EXCEPTION;

            var result = _quotesController.Delete(_defaultId) as StatusCodeResult;
            Assert.Equal(500,result.StatusCode);
        }
        #endregion
    }
}
